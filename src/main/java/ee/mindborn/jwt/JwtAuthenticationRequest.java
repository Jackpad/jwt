package ee.mindborn.jwt;

import java.io.Serializable;

public class JwtAuthenticationRequest implements Serializable {

    private static final long serialVersionUID = -8445943548965154778L;

    private String user;
    private String password;

    public JwtAuthenticationRequest() {
    }

    public JwtAuthenticationRequest(String user, String password) {
        this.setUser(user);
        this.setPassword(password);
    }

    public String getUser() {
        return this.user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
