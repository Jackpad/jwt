package ee.mindborn.service;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Jevgeni on 18.08.2016.
 */
@Service
public class UserServiceImpl implements UserDetailsService {

    private Map<String, User> users;

    @PostConstruct
    public void init() {
        users = new HashMap();
        User admin = new User("admin", "admin", Stream.of(new SimpleGrantedAuthority("ROLE_ADMIN")).collect(Collectors.toList()));
        User sysadmin = new User("sysadmin", "sysadmin", Stream.of(new SimpleGrantedAuthority("ROLE_SYSADMIN")).collect(Collectors.toList()));
        users.put(admin.getUsername(), admin);
        users.put(sysadmin.getUsername(), sysadmin);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = users.get(username);
        if (user == null) {
            throw new UsernameNotFoundException(String.format("No user found with username '%s'.", username));
        } else {
            return new User(user.getUsername(), user.getPassword(), user.getAuthorities());
        }
    }
}
